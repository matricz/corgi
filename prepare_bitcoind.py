#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

import os
import requests
import utils
import random
import time
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException

images = dict()

rpc_user = "vestal"
rpc_password = "1qaz2wsx3edc4rfv5tgb"

# prepare the nodes and connections
def prepare_nodes(run_id):

    instances = utils.get_instances(run_id)

    # sed custom values to the preyer
    myip = utils.get_public_ip()
    os.system("cp script_template.sh {}/script.sh".format(run_id))
    os.system('sed -i "s/MYPUBLICIP/{}/g" {}/script.sh'.format(myip, run_id))

    # wait some more for sshd to warm up
    time.sleep(60)

    def prepare_node(instance):
        ip = instance["ip"]
        region_name = instance["region"]
        keyfile = "{}/{}.pem".format(run_id, region_name)
        # after several experiments you could run into the same ips twice. ssh would complain.
        # we'll remove newly assigned ips from locally knows_hosts
        os.system('ssh-keygen -f "~/.ssh/known_hosts" -R "{}"'.format(ip))

        # copy over the relevant scripts to the instances
        os.system("scp -oStrictHostKeyChecking=no -i {} {}/script.sh ubuntu@{}:~/script.sh".format(keyfile, run_id, ip))
        os.system("scp -oStrictHostKeyChecking=no -i {} ./record.py ubuntu@{}:~/record.py".format(keyfile, ip) )
        
        # The following 'nohup...' will trigger the remote execution of the remote script while yielding immidiately
        time.sleep(10) # let the OS start up completely
        os.system("ssh -oStrictHostKeyChecking=no -i {} ubuntu@{} 'nohup bash ~/script.sh &> script.log &'".format(keyfile, ip))
    
    print("Copying over scripts to the nodes")
    tasks = []
    for instance in instances:
        tasks.append([prepare_node, instance])
    utils.create_multiverse(tasks)


def connect_nodes(run_id, num_nodes, num_connections):
    instances = utils.get_instances(run_id)

    print("waiting for all the nodes to be ready")
    for instance in instances:
        ready = False
        while ready is not True:
            print("Trying to connect to {}".format(instance))
            try:
                url = "http://{}:{}@{}:8332".format(rpc_user, rpc_password, instance["ip"])
                rpc_connection = AuthServiceProxy(url)
                print(rpc_connection.getbestblockhash())
                print("Connection to {} successful".format(instance))
                ready = True
            except Exception as e:
                print(e)
                time.sleep(10)
                pass

    time.sleep(10)

    topology = utils.get_random_topology(num_nodes, num_connections)

    def connect_node(node_num, num_connections=num_connections):

        for edge_num in range(num_connections):
            from_node, to_node = topology[node_num*num_connections + edge_num]
            url = "http://{}:{}@{}:8332".format(rpc_user, rpc_password, instances[from_node]["ip"])
            print("connecting {} to {}  ({})".format(instances[from_node], instances[to_node], url))
            rpc_connection = AuthServiceProxy(url)
            host = instances[to_node]["ip"] + ":8333"
            try: 
                print(rpc_connection.addnode(host, "add"))
            except Exception as error:
                print(error)
            print(rpc_connection.addnode(host, "onetry"))

    tasks = []
    for num_node in range(num_nodes):
        tasks.append([connect_node, num_node])
    utils.create_multiverse(tasks)


# prepare the utxos, several for each node
def prepare_utxos(run_id):
    instances = utils.get_instances(run_id)

    # prepare 20 utxos for each node
    for iteration in range(20):
        for node in instances:
            url = "http://{}:{}@{}:8332".format(rpc_user, rpc_password, node["ip"])
            rpc_connection = AuthServiceProxy(url)
            print( "Round {}: {}".format(iteration, rpc_connection.generate(1)) )
            # give it time to propagate (there's no delay so it should propagate in some 0.8s)
            # even if some of these get orphaned it's no big deal
            time.sleep(1)
    pass    

def copy_and_run_node_actions(run_id):
    instances = utils.get_instances(run_id)

    def equip_single_node(instance):
        ip = instance["ip"]
        region_name = instance["region"]
        keyfile = "{}/{}.pem".format(run_id, region_name)
        # copy over the relevant scripts to the instances
        os.system("scp -oStrictHostKeyChecking=no -i {} node_actions.py ubuntu@{}:~/node_actions.py".format(keyfile, ip) )
        os.system("ssh -oStrictHostKeyChecking=no -i {} ubuntu@{} 'nohup python3 ~/node_actions.py &> node_actions.log &'".format(keyfile, ip) )

    tasks = []
    for instance in instances:
        tasks.append([equip_single_node, instance])
    utils.create_multiverse(tasks)    


# By default this will solve blocks in descending times
# from 15 minutes down to 14 minutes, ... down to 1 minute
def solve_some_blocks(run_id):
    instances = utils.get_instances(run_id)

    interval = 4
    while interval > 0:
        # a random node will solve the block
        node_num = random.randrange(len(instances))
        node = instances[node_num]
        url = "http://{}:{}@{}:8332".format(rpc_user, rpc_password, node["ip"])
        rpc_connection = AuthServiceProxy(url)
        blockhash = rpc_connection.generate(1)
        print("Solved block {} on node {}, waittime == {}".format(blockhash, node, interval))
        
        time.sleep(interval * 60)
        interval = interval -1
