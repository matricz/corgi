#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

####################################################################
# corgi                                                            #
#                                                                  #
# A framework for testing emergent properties of a bitcoin network #
####################################################################               

# Main author: mtrycz (2020)
# This framework is inspired by and dedicated to aphyr


# PREREQUISITES

# an account at aws (don't use this same account for other work, you might loose it)
# ~/.aws/credentials file (or any other valid credentials in https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html#guide-configuration)
# build/src/bitcoind prebuilt
#
# OPTIONAL if you run on mainnet:
# A recent snapshot of a pruned blockchain, zipped and downloadable at the location in blkchain_dl_link

import configs
import prepare_aws
import prepare_bitcoind
import gather
import process_data
import cleanup
import utils
import math


# some globals
regional_clients = dict()
security_groups = dict()


def run_full_test():
    try: 
        prepare_the_test()
        
        gather.gather()
        process_data.process_all()
    
    except Exception as error:
        print(error)
        raise error
    
    finally:
        cleanup.cleanup()
        

# prepare the instances, nodes and connections; also balances
def prepare_the_test():

    if configs.i_accept_the_responsibility_of_running_this_script != True:
        raise Exception("With great power comes great responsibility. Do you accept it?") 

    run_id = utils.create_new_run_id()

    regional_clients = prepare_aws.create_aws_regional_clients()
    prepare_aws.create_security_groups(regional_clients, run_id, security_groups)
    prepare_aws.create_aws_instances(run_id, regional_clients, security_groups, configs.num_nodes)
    prepare_bitcoind.prepare_nodes(run_id)
    prepare_bitcoind.connect_nodes(run_id, configs.num_nodes, configs.num_connections)
    prepare_bitcoind.prepare_utxos(run_id)
    prepare_bitcoind.copy_and_run_node_actions(run_id)
    prepare_bitcoind.solve_some_blocks(run_id)


run_full_test()


# WARINING - END OF THE WORLD AHEAD
# DON'T UNCOMMENT THE FOLLOWING LINE
# cleanup.armageddon()