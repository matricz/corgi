#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

import os
import csv
import utils
import configs

# prepare those juicy charts (for now only a csv)
def process_data(run_id=None, type="hashtx"):
    if run_id is None:
        run_id = utils.read_ceremony_id()
    filenames = os.listdir(run_id)
    headers  = [] # for the csv
    transactions = dict()
    pattern = "{}.log".format(type)

    for filename in filenames:
        if filename.endswith(pattern):
            headers.append(filename)
            with open(run_id+"/"+filename) as f:
                for line in f:
                    line = line.strip()
                    values = line.split()
                    txid = values[0]
                    time = float(values[1])

                    if time > 1590946702:            # if you want arbitrary time limits, put them here
                        if txid not in transactions:
                            transactions[txid] = dict()
                            # transactions[txid]["txid"] = txid

                        # most transactions are reported twice: once when they are addmitted to the mempool
                        # and then when they are included in a block. 
                        # we're interested only in the first occurrence 
                        if filename not in transactions[txid]:
                            transactions[txid][filename] = time

    complete_transactions = dict()
    for transaction in transactions:
        if len(transactions[transaction]) == configs.num_nodes:
            complete_transactions[transaction] = transactions[transaction]
    transactions = complete_transactions

    for transaction in transactions:
        min = 9999999999 # year 2286
        max = 0
        minnode = ""
        maxnode = ""
        for node in transactions[transaction]:
            time = float(transactions[transaction][node])
            if time < min:
                min = time
                minnode = node[0]
            if time > max:
                max = time
                maxnode = node[0]
        transactions[transaction]["min"] = min
        transactions[transaction]["max"] = max
        transactions[transaction]["diff"]= max - min

    headers.insert(0, "txid")
    headers.append("min")
    headers.append("max")
    headers.append("diff")


    output_filename = "{}/{}propagation.csv".format(run_id, type)
    with open(output_filename, "w") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=headers)
        writer.writeheader()
        for transaction in transactions:
            transactions[transaction]["txid"] = transaction # prepend a column for txids
            writer.writerow(transactions[transaction])

def process_transactions(run_id=None):
    process_data(run_id, "hashtx")

def process_blocks(run_id=None):
    process_data(run_id, "hashblock")

def process_all(run_id=None):
    process_transactions(run_id)
    process_blocks(run_id)

process_all()