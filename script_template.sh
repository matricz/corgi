#!/bin/bash

#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

# This will be copied over 
# and ran locally on each instance

### This for a default installation, you probably want a custom binary
# sudo add-apt-repository ppa:bitcoin-cash-node/ppa
# sudo apt-get update
# sudo apt-get install bitcoind 

sleep 10

sudo apt-get update
sudo apt-get upgrade -y -q
sudo apt-get install -y -q build-essential cmake git libboost-chrono-dev libboost-filesystem-dev libboost-test-dev libboost-thread-dev libevent-dev libminiupnpc-dev libssl-dev libzmq3-dev ninja-build python3 libdb-dev libdb++-dev

sudo apt-get install -y -q python3-pip unzip
pip3 install zmq python-bitcoinrpc

### currently the binary is downloaded and extracted from the official release
### If a custom binary is to be tested, update this accordingly

wget -q https://mtrycz-test.s3.eu-central-1.amazonaws.com/MR143 -O bitcoind
wget -q https://mtrycz-test.s3.eu-central-1.amazonaws.com/bitcoin-cli
chmod +x bitcoind
chmod +x bitcoin-cli
# tar -zxvf bitcoin-cash-node-0.21.2-x86_64-linux-gnu.tar.gz

### the blockchain snapshot is downloaded and unzipped from 
### a file manually uploaded to s3 and made public
# wget https://mtrycz-test.s3.eu-central-1.amazonaws.com/prunedblockchain.zip
# unzip prunedblockchain.zip -d ./.bitcoin


./bitcoind \
    -daemon \
    -port=8333 \
    -zmqpubhashtx=tcp://127.0.0.1:28905 \
    -zmqpubhashblock=tcp://127.0.0.1:28905 \
    -server \
    -rpcuser=vestal \
    -rpcpassword=1qaz2wsx3edc4rfv5tgb \
    -rpcallowip=MYPUBLICIP \
    -rpcport=8332 \
    -connect=0 \
    -listen=1 \
    -regtest
    # -prune=550 \

### record.py will log all incoming blocks and transactions

python3 record.py

