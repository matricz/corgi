#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

# !!! DANGER !!!
# RUNNING THIS SCRIPT WILL LOOSE YOU MONEY AND DELETE YOUR AWS INSTANCES
i_accept_the_responsibility_of_running_this_script = False # Check out the MIT License. You have been warned.

# Configuration
num_nodes = 256
num_connections = 8 # or int(math.sqrt(num_nodes))

# If you'll be running on the mainnet, it's best if you have a snapshot of a pruned blockchain uploaded somewhere
blkchain_dl_link = "https://mtrycz-test.s3.eu-central-1.amazonaws.com/prunedblockchain.zip" # make this a zip file

image_name = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-20200423"