This experiment was ran on friday 29/05/2020 evening. 

It was ran mainly to check the correctness of the framework. 

A total of 16 nodes in 10 locations were ran over a period of a couple of hours. 

Each node recorded a precise timestamp of when each transaction was received.

The times were then compared, and the difference between the first and last seen calculated.

The following chart shows the propagation times for each transaction in ascending order. On the live BCH mainnet the propagation time is under 2 seconds for most transactions.

![Propagation times](200530propagation.jpg)