#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.
 
import time
import asyncio
import zmq
import zmq.asyncio
import socket
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException

# DEPRECATION NOTICE:
# various bitcoind versions support the `-logtimemicros` flag
# which is very convenient. Future versions will simply parse the debug.log files
# for now this was made to obtain sub-second resolution for propagation times

# bitcoind has second resolution for transactions, which is not enough for our case
# we'll connect to the ZMQ endpoint to log sub-second times with python 

rpc_user = "vestal"
rpc_password = "1qaz2wsx3edc4rfv5tgb"

def main():

    # Wait for the IBD to finish before collecting transactions
    ibd = True
    while ibd is True:
        try:
            rpc_connection = AuthServiceProxy("http://{}:{}@127.0.0.1:8332".format(rpc_user, rpc_password))
            bcinfo = rpc_connection.getblockchaininfo()
            ibd = bcinfo["initialblockdownload"]
            print("{}: bitcoind is still in IBD".format(time.time()))
            time.sleep(1)
        except Exception as error:
            print("{}: bitcoind has not responded with error: {}".format(time.time(), error))
            time.sleep(1)
 
                

    # Then start logging the transaction info
    loop = asyncio.new_event_loop()  # create an event loop for this thread
    loop.run_until_complete(receive())


async def receive():
    print("ZMQ task starting")
    ctx = zmq.asyncio.Context()
    sock = ctx.socket(zmq.SUB)
    host = socket.gethostname()
    url = 'tcp://127.0.0.1:28905'
    sock.connect(url)
    sock.setsockopt(zmq.SUBSCRIBE, b'hashtx')
    sock.setsockopt(zmq.SUBSCRIBE, b'hashblock')

    stop_flag = False
    while not stop_flag:
        if await sock.poll(timeout=100):  # timeout here is in milliseconds for poll
            now = time.time()  # mark the time now because if poll returned true, the msg was indeed ready
            # we got a message, grab it and log its time
            msg = await sock.recv_multipart()  # waits for msg to be ready -- it should return immediately
            name, hash, _ = msg  # messages are [name, txid_bytes, some_extra_stuff]
            hashhex = hash.hex()
            if name == b'hashtx':
                queuetype = "hashtx"
            else:
                queuetype = "hashblock"

            filename = "{}.log".format(queuetype)
            with open(filename, "a") as myfile:
                myfile.write("{} {}\n".format(hashhex, now))

main()